# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requires = [
    'nose',
]

setup(
    name='MonMonCheckLib',
    version='0.0',
    description='Just the check code for use in probe servers',
    author='Tim Freund',
    author_email='tim@freunds.net',
    license = 'MIT',
    url='https://bitbucket.org/timfreund/monmon-checklib',
    install_requires=requires,
    packages=['mmcl'],
    include_package_data=True,
)
