from unittest import TestCase
from mmcl.checks import http

from http.server import HTTPServer, SimpleHTTPRequestHandler
import threading

http_server = None
http_thread = None

def setUpModule():
    global http_server
    global http_thread
    http_server = HTTPServer(('0.0.0.0', 8001), SimpleHTTPRequestHandler,
                            bind_and_activate=True)
    http_thread = threading.Thread(target=http_server.serve_forever)
    http_thread.start()

def tearDownModule():
    http_server.shutdown()

class TestHTTP(TestCase):
    def test_valid_status_code(self):
        response = http.execute("http://localhost:8001/", status_code=200)
        self.assertIsNotNone(response.starttime)
        self.assertIsNotNone(response.runtime)
        self.assertEqual(0, len(response.faults))

    def test_404_status_code(self):
        response = http.execute("http://localhost:8001/does_not_exist", status_code=404)
        self.assertEqual(0, len(response.faults))

    def test_broken_link(self):
        response = http.execute("http://localhost:8001/should_exist_but_does_not", status_code=200)
        self.assertNotEqual(0, len(response.faults))

    def test_nxdomain(self):
        response = http.execute("http://doesnotexist.freunds.net", status_code=200)
        self.assertNotEqual(0, len(response.faults))

    def test_wrong_port(self):
        response = http.execute("http://localhost:81", status_code=200)
        self.assertNotEqual(0, len(response.faults))
