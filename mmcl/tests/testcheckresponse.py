from mmcl import CheckResponse
from datetime import datetime, timedelta
from unittest import TestCase

class TestCheckResponse(TestCase):
    def test_json(self):
        cr = CheckResponse()
        cr.starttime = datetime.now()
        cr.runtime = timedelta(microseconds=7654).total_seconds()

        cr2 = CheckResponse.from_json(cr.to_json())
        self.assertEqual(cr2.to_json(), cr.to_json())
