import inspect

def get_check_modules():
    from mmcl import checks
    # filter(lambda obj: inspect.ismodule(obj), [getattr(checks, name) for name in dir(checks)])
    return [x for x in inspect.getmembers(checks) if inspect.ismodule(x[1])]

def get_check_parameters(function):
    params = {}
    sig = inspect.signature(function)
    for name, param in sig.parameters.items():
        details = {
            'required': True,
            'default': None,
            'parameter_type': 'STR',
        }
        params[name] = details
        if param.default != inspect._empty:
            details['required'] = False
            details['default'] = param.default
        if param.annotation != inspect._empty:
            details['parameter_type'] = param.annotation.__name__.upper()

    return params

def get_check_definitions():
    definitions = {}
    for name, mod in get_check_modules():
        definitions[name] = get_check_parameters(mod.execute)
    return definitions

def register_definitions():
    import json
    from urllib.error import URLError
    from urllib.request import Request, urlopen

    defs = get_check_definitions()
    for name, params in defs.items():
        payload = {}
        payload['code_name'] = name
        payload['display_name'] = name.upper()
        payload['metaparameters'] = []

        for pname, meta in params.items():
            meta['name'] = pname
            meta['display_name'] = pname.upper()
            meta['description'] = "%s description goes here" % pname.upper()
            payload['metaparameters'].append(meta)

        json_payload = json.dumps(payload, sort_keys=True, indent=2)
        print(json_payload)
        # request = Request('http://localhost:8000/api/checktypes/',
        #                   data=bytearray(json_payload, "UTF-8"),
        #                   headers={'Content-Type': 'application/json'})
        # try:
        #     response = urlopen(request)
        # except URLError as ue:
        #     print(ue)
        #     import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # print(response.read())
