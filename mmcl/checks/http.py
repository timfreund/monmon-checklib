from mmcl import CheckRequest, CheckResponse, time_response
from datetime import datetime
from urllib.error import URLError, HTTPError
from urllib.request import urlopen

@time_response
def execute(url, status_code:int=200, string_check=None,
            auth_username=None, auth_password=None):
    response = CheckResponse()
    faults = []

    status_code = int(status_code)

    tested_status = None
    tested_content = None
    try:
        http_response = urlopen(url)
        tested_status = http_response.status
        tested_content = http_response.read()
    except HTTPError as he:
        # People may check to ensure that broken pages are broken
        # in an expected way.
        tested_status = he.code
        tested_content = he.read()
    except URLError as ue:
        response.faults.append(ue.reason)
        return response

    if tested_status != status_code:
        faults.append("status_code mismatch: %d != %d" % (tested_status,
                                                          status_code))
    if string_check:
        if tested_content.find(string_check) == -1:
            faults.append("string not found: " % string_check)

    if not len(faults):
        response.successful = True
    response.faults = faults
    return response


