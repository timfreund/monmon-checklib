from datetime import datetime, timedelta
from functools import wraps
import importlib
import json

def execute(check_request):
    if isinstance(check_request, str):
        check_request = CheckRequest.from_json(check_request)

    module_name = "mmcl.checks.%s" % check_request.check_type
    check_mod = importlib.import_module(module_name)
    check_response = check_mod.execute(**check_request.params)
    # these three lines feel like a hack...
    check_response.request_id = check_request.request_id
    check_response.check_type = check_request.check_type
    check_response.request_params = check_request.params
    return check_response

class CheckRequest(object):
    def __init__(self, check_type=None, request_id=None, **params):
        self.check_type = check_type
        self.request_id = request_id
        self.params = params

    @classmethod
    def from_json(klass, json_string):
        crdict = json.loads(json_string)
        cr = CheckRequest()
        cr.__dict__ = crdict
        return cr

class CheckResponse(object):
    def __init__(self, request=None, check_type=None, request_id=None,
                 successful=False, runtime=None, starttime=None,
                 faults=[], **request_params):
        if request:
            request_id = request.request_id
            check_type = request.check_type
        self.request_id = request_id
        self.check_type = request

        self.successful = successful
        self.faults = faults
        self.starttime = starttime
        self.runtime = runtime

        self.request_params = request_params

    @classmethod
    def from_json(klass, json_string):
        crdict = json.loads(json_string)
        crdict['starttime'] = datetime.strptime(crdict['starttime'], '%Y-%m-%dT%H:%M:%S.%f')
        cr = CheckResponse()
        cr.__dict__ = crdict
        return cr

    def to_json(self):
        serializable = self.__dict__.copy()
        serializable['starttime'] = serializable['starttime'].isoformat()
        return json.dumps(serializable)

def time_response(execute_check):
    @wraps(execute_check)
    def timed_execution(*args, **kwargs):
        starttime = datetime.utcnow()
        response = execute_check(*args, **kwargs)
        runtime = datetime.utcnow() - starttime
        response.runtime = runtime.total_seconds()
        response.starttime = starttime
        return response
    return timed_execution
